const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

// GIVE THE MULTIPLY TABLE OF AN INTEGER
readline.question("Choose a number : ", (input) => {
    for (let i = 1; i <= 10; ++i) {
        console.log(`${input} x ${i} = ${input * i}`);
    }
})