const congratsOrNot = (note) => {
    if (note >= 15) {
        console.log("Congrats");
    } else if (note >= 10) {
        console.log("So So");
    } else {
        console.log("Get out of here");
    }
} 

congratsOrNot(1);  // Expected output : "Get out of here"
congratsOrNot(10); // Expected output : "So So"
congratsOrNot(15); // Expected output : "Congrats"