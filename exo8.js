// GET MAX VALUE WITH UNLIMITEED ARGUMENTS
const max = (...arguments) => {
    let inputs = arguments;
    console.log(Math.max(inputs));
}

// GET MIN VALUE
const min = (...arguments) => {
    let inputs = arguments;
    console.log(Math.min(inputs));
}

max(1, 2, 3); // Expected output : 3
max(1, 2);    // Expected output : 2
min(1, 2, 3); // Expected output : 1