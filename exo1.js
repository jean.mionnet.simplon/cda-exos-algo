// ALL USER INPUTS
let inputs = [];

const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

const recursiveReadline = (index) => {
    // WHILE THE AMOUNT OF INPUTS IS NOT SUFFICIENT
    if (index < 20) {
        // SEND A QUESTION
        rl.question("Enter a number : ", (input) => {
            // VERIFY IF IT'S A NUMBER
            if (!isNaN(input)) {
                // ADD 1 TO THE AMOUNT AND STORE THE INPUT
                ++index;
                inputs = [...inputs, input];
            } else {
                // SEND AN ERROR MESSAGE
                console.log(`"${input}" is not a number`);
            }
            // GET BACK TO THE FUNCTION
            recursiveReadline(index);
        })
    } else {
        const maxInput = Math.max(...inputs);
        const maxInputIndex = inputs.indexOf(maxInput.toString()) + 1;

        let suffix = "";
        maxInputIndex > 1 ? suffix = "th" : suffix = "st"; 

        console.log(`${maxInput} is the biggest int, it was your ${maxInputIndex}${suffix} input`);
    }
}

recursiveReadline(0);