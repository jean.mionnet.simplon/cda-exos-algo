const factor = (n) => {
    const temp = n;
    for (let i = 1; i <= temp; ++i) {
        n *= i;
    }
    return n;
}

const getChances = (n, p) => {
    const x = Math.round(factor(n) / factor(n - p));
    const y = Math.round(factor(n) / (factor(p) * factor(n - p)));
    console.log(`Une chance sur ${x} de gagner dans l'ordre\nUne chance sur ${y} de gagner dans le désordre`);
}

getChances(12, 4);