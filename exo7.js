const int = [1, 2, 3, 4, 5, 6];

// SUM OF ARRAY'S INTEGERS DIVIDED BY ITS LENGTH
const average = (int.reduce((a, b) => a + b, 0) / int.length);

console.log(average); // Expected output : 3.5