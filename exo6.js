// CONCAT FUNCTION
const concat = (str1, str2) => {
    return str1.concat(str2);
}

// LEXICOGRAPHIC FUNCTION
const lexiocographicOrder = (...args) => {
    return args.sort((a, b) => a.localeCompare(b, 'fr'));
}

// REMOVE A NUMBER OF CHARACTERS OF A STRING FROM AN INDEX
const cutString = (str, index, length) => {
    return `${str.substr(0, index)}${str.substr(index + length, str.length)}`;
}

console.log(concat("test", "test"));                                       // Expected output : testtest
console.log(lexiocographicOrder("Jean", "Mionnet", "Développeur", "Web")); // Expected output : ['Développeur', 'Jean', 'Mionnet', 'Web']
console.log(cutString("darksouls", 1, 3));                                 // Expected output : dsouls
