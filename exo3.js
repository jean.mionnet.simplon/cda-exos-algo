const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

// GIVE THE FACTORIAL OF A NUMBER
readline.question("Choose a number : ", (input) => {
    let factorial = 1;

    for (let i = 1; i <= input; ++i) {
        factorial *= i;
    }

    console.log(factorial);
})